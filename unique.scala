object Kata {

  def uniqueInOrder[T](xs: Iterable[T]): Seq[T] = {
    
    def deDupe[T](xs: Iterable[T]): Seq[T] = {
      if (xs.isEmpty) Seq()
      else xs.head +: deDupe(xs.tail.dropWhile(_ == xs.head))
    }
    
    deDupe(xs)
  }
}