# DESCRIPTION:

Implement the function `unique_in_order` which takes as argument a sequence and returns a list of items without any elements with the same value next to each other and preserving the original order of elements.

For example:

    uniqueInOrder("AAAABBBCCDAABBB")   == List('A', 'B', 'C', 'D', 'A', 'B')
    uniqueInOrder("ABBCcAD")           == List('A', 'B', 'C', 'c', 'A', 'D')
    uniqueInOrder(List(1, 2, 2, 3, 3)) == List(1, 2, 3)
